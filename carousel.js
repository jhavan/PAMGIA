$(document).ready(function() {
    $(".owl-carousel").owlCarousel();
});
$(".owl-carousel").owlCarousel({

    responsive: {
        300: {
            stagePadding: 10,
            margin: 185
        },

        // breakpoint from 0 up
        1366: {
            stagePadding: 180,
            margin: 110,
        },
        // breakpoint from 480 up
        1600: {
            stagePadding: 190,
            margin: 120,
        },
        1800: {
            stagePadding: 230,
            margin: -1,
        },
    },
});

// <svg class="tiktok-1h6rn3a-StyledChevronLeftOffset elgg5o63" width="32" height="32" viewBox="0 0 48 48" fill="currentColor" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M4.58579 22.5858L20.8787 6.29289C21.2692 5.90237 21.9024 5.90237 22.2929 6.29289L23.7071 7.70711C24.0976 8.09763 24.0976 8.7308 23.7071 9.12132L8.82843 24L23.7071 38.8787C24.0976 39.2692 24.0976 39.9024 23.7071 40.2929L22.2929 41.7071C21.9024 42.0976 21.2692 42.0976 20.8787 41.7071L4.58579 25.4142C3.80474 24.6332 3.80474 23.3668 4.58579 22.5858Z"></path></svg>
var background = document.getElementById("bg");
var title = document.getElementById("title");
var link = document.querySelector("#theme-link");

var titleArray = [{
        description: "Informações, painéis e mapas de ações da fiscalização e proteção ambiental realizada pelas equipes do Ibama. ",
        background: "url('images/fisca.jpg')",
        url: "https://pamgia.ibama.gov.br/home/?page=Fiscalização-e-Prot-Ambiental",
    },
    {
        description: "Informações, painéis e mapas do desmatamento e ações de combate ao desmatamento realizado pelo Ibama em todo o território nacional. ",
        background: "url('images/desmat.jpg')",
        url: "https://pamgia.ibama.gov.br/home/?page=Combate-ao-Desmatamento&views=Exibir-6",
    },
    {
        description: " Informações, painéis e mapas contendo dados sobre as estratégias e ações de manejo integrado do fogo, abordagem que inclui o manejo do fogo, a ecologia do fogo e a cultura do fogo.",
        background: "url('images/prevfogobg.jpg')",
        url: "https://pamgia.ibama.gov.br/home/?page=Manejo-Integrado-do-Fogo",
    },


    {
        description: "Informações, painéis e mapas contendo dados sobre a relação de acidentes e emergências ambientais registradas no Brasil e as estratégias de assistência e apoio operacional às instituições públicas e à sociedade. ",
        background: "url('images/emergencia.jpg')",
        url: "https://pamgia.ibama.gov.br/home/?page=Acidentes-Emergências-Ambientais",
    },

    {
        description: "Informações, painéis e mapas sobre os registros e monitoramento das licenças ambientais federais emitidas pelo Ibama. ",
        background: "url('images/licenciamento.jpg')",
        url: "https://pamgia.ibama.gov.br/home/?page=Licenciamento-Ambiental",
    },
    {
        description: "Informações, painéis e mapas das autorizações de utilização de matéria prima florestal, manejo florestal, controle da origem da madeira, do carvão e de outros produtos ou subprodutos florestais, sob coordenação, fiscalização e regulamentação do Ibama. ",
        background: "url('images/flora-madeira.jpg')",
        url: "https://pamgia.ibama.gov.br/home/?page=Flora-e-Madeira",
    },


    {
        description: "Informações, painéis e mapas sobre o Monitoramento do Uso da Fauna e Recursos Pesqueiros no Brasil, nas áreas de competência federal do Ibama. ",
        background: "url('images/faunabg.jpg')",
        url: "https://pamgia.ibama.gov.br/home/?page=Fauna",
    },
    {
        description: " Informações, painéis e mapas contendo dados sobre recuperação Ambiental como forma de Reparação por Danos ou Compensação por Impactos Ambientais ",
        background: "url('images/recuperacaobg.jpg')",
        url: "https://pamgia.ibama.gov.br/home/?page=Recuperação-Ambiental",
    },

    {
        description: "Informações, painéis e mapas sobre o  Cadastro Técnico Federal de Atividades e Instrumentos de Defesa Ambiental  que é um instrumento da Política Nacional do Meio Ambiente previsto no Art 9º inciso VIII da  Lei nº 6.938 de 31 de agosto de 1981. ",
        background: "url('images/cadastro.png')",
        url: "https://pamgia.ibama.gov.br/home/?page=Cad-Técnico-Federal",
    },
    {
        description: "Informações, painéis e mapas sobre o monitoramento da emissão e resíduos de competência do Ibama. ",
        background: "url('images/residuos.png')",
        url: "https://pamgia.ibama.gov.br/home/?page=Emissão-de-Resíduos",
    },
    {
        description: "Informações, painéis e mapas sobre o monitoramento, avaliação, registro e comercialização de produtos químicos e biológicos (agrotóxico) realizado pelas equipes do Ibama. ",
        background: "url('images/quimicos.png')",
        url: "https://pamgia.ibama.gov.br/home/?page=Químicos-e-Biológicos",
    },

    {
        description: "Informações, painéis e mapas sobre as ações de educação ambiente em todo o Brasil.",
        background: "url('images/educacao.png')",
        url: "https://pamgia.ibama.gov.br/home/?page=Educação-Ambiental",
    },
    {
        description: "Informações, painéis e mapas sobre o monitoramento dos ambientes costeiros e marinho.",
        background: "url('images/marinho.jpg')",
        url: "https://pamgia.ibama.gov.br/home/?page=Ambiente-Costeiro-Marinho",
    },
];
var description = document.getElementById("desc");
var next = document.getElementById("next");
var previous = document.getElementById("prev");
var progressBar = document.getElementById("progress");
var sliderNumber = document.getElementById("number");
var val = -1;

var cardsElements = document.querySelectorAll("div[value]");

function selectCard() {
    link.style.display = "block";
    let activeCard = document.querySelector(`div[value='${val}']`);

    title.innerText = activeCard.querySelector(`div.card-title`).innerText;
    description.innerText = titleArray[val].description;
    link.href = titleArray[val].url;
    background.style.backgroundImage = titleArray[val].background;
    progressBar.style.marginLeft = progressBarWidth * val + "%";

    sliderNumber.innerText = "0" + (val + 1);
    if (val > 8) {
        sliderNumber.innerText = val + 1;
    }

    for (var i = 0; i < cardsElements.length; i++) {
        cardsElements[i].classList.remove("image-card-active");
        cardsElements[i].classList.add("image-card");
    }
    activeCard.classList.add("image-card-active");
    activeCard.classList.remove("image-card");
}
for (var i = 0; i < cardsElements.length; i++) {
    cardsElements[i].onclick = function(event) {
        val = Number(this.getAttribute("value"));
        selectCard();
        $(".owl-carousel").trigger('to.owl.carousel', [val, 500, true]);
    };
}

var progressBarWidth = 100 / cardsElements.length;
progressBar.style.width = progressBarWidth + "%";
progressBar.style.transition = "margin 500ms ease-in-out";
background.style.transition = "background-image 0.6s ease-in-out";

next.onclick = function() {
    val++
    selectCard();
    $(".owl-carousel").trigger('to.owl.carousel', [val, 500, true]);
};

previous.onclick = function() {
    val--;
    selectCard();
    $(".owl-carousel").trigger('to.owl.carousel', [val, 500, true]);
};
